import React, { createContext, useEffect, useState } from "react";
import { cache } from "../services/cache";

export const ProductContext = createContext();

export const ProductProvider = (props) => {
  //States
  const { getCache } = cache();
  const [products, setProducts] = useState(() => {
    const storedProducts = getCache("products");
    return storedProducts.data ? storedProducts.data : [];
  });

  //Add product function
  const addProduct = (product) => {
    setProducts((productsList) => [...productsList, product]);
  };

  //Data persistence
  useEffect(() => {
    const { getCache } = cache();

    const storedProducts = getCache("products");

    if (storedProducts !== null && storedProducts.data.length > 0) {
      setProducts(storedProducts.data);
    } else {
      setProducts([]);
    }
  }, []);

  //setting products in LS
  useEffect(() => {
    const { setCache } = cache();

    setCache(products, "products");
  }, [products]);

  return (
    <ProductContext.Provider value={{ products, addProduct }}>
      {props.children}
    </ProductContext.Provider>
  );
};
