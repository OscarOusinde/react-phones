
export const cache = () =>{
    
const getCache = (key) => {
    const cacheData = JSON.parse(localStorage.getItem(`${key}`));

   if(cacheData.expirationTime < new Date().getTime()) {
    // alert('Your cart has expired')
    return {data:[]}
   }

    return cacheData
}


const setCache = (value, key) => {

    const expirationTime = new Date().getTime() + (60*60*1000);

    const dataToLS = {data: value, expirationTime,}

    localStorage.setItem(`${key}`, JSON.stringify(dataToLS));
}

return {
    getCache,
    setCache
}

}