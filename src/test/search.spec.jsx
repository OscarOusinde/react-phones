import React from "react";
import { describe, it, expect, jest } from "@jest/globals";
import { fireEvent, render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Search } from "../components/Search";

describe("Search bar specs", () => {
  it("should display a search input", () => {
    //Arrange
    const filterSearch = () => {};

    //Act
    render(<Search filterSearch={filterSearch} />);

    //Assert
    const inputElement = screen.getByRole("textbox");
    expect(inputElement).toBeInTheDocument();
  });
  it("should type something on the search bar", () => {
    //Arrange
    const filterSearch = () => {};
    const searching = "C";

    //Act
    render(<Search filterSearch={filterSearch} />);

    const inputSearch = screen.getByRole("textbox");

    fireEvent.change(inputSearch, { target: { value: searching } });

    //Assert
    expect(inputSearch.value).toContain(searching);
  });
  it("should call filterSearch when something is searched", () => {
    //Arrange
    const filterSearch = jest.fn();
    const searching = "test";

    //Act
    render(<Search filterSearch={filterSearch} />);
    const inputSearch = screen.getByRole("textbox");
    fireEvent.change(inputSearch, { target: { value: searching } });

    //Assert
    expect(filterSearch).toHaveBeenCalled();
    expect(filterSearch).toHaveBeenCalledWith(searching);
  });
});
