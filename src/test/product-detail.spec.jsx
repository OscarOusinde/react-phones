import React from "react";
import { describe, it, expect } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom/extend-expect";
import { HashRouter, Routes, Route } from "react-router-dom";
import { ProductProvider } from "../contexts/ProductContext";
import { ProductDetail } from "../components/ProductDetail";

const renderWithRouter = (component) => {
  render(
    <ProductProvider>
      <HashRouter>
        <Routes>
          <Route path="/" element={component} />
          <Route path="/products/:productName/:productId" element={component} />
        </Routes>
      </HashRouter>
    </ProductProvider>
  );
};

describe("ProductDetail component specs", () => {
  it("should display an image of the product", () => {
    //Arrange

    //Act
    renderWithRouter(<ProductDetail />);

    const productImage = screen.getByTestId("product-image");

    //Assert
    expect(productImage).toBeInTheDocument();
  });
});
