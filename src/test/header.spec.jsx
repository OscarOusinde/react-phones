import React from "react";
import { describe, it, expect } from "@jest/globals";
import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Header } from "../components/Header";
import { HashRouter } from "react-router-dom";
import { ProductProvider } from "../contexts/ProductContext";

const renderWithRouter = () => {
  render(
    <ProductProvider>
      <HashRouter>
        <Header />
      </HashRouter>
    </ProductProvider>
  );
};
describe("Header component specs", () => {
  it("should display a logo, counter and breadcrums", () => {
    //Arrange

    //Act
    renderWithRouter(<Header />);

    //Assert
    const headerElement = screen.getByRole("banner");
    const logoElement = screen.getByAltText("logoIpsum");
    const productCounter = screen.getByTestId("product-count");
    const breadcrumbsElement = screen.getByRole("navigation");

    expect(headerElement).toBeInTheDocument();
    expect(logoElement).toBeInTheDocument();
    expect(productCounter).toBeInTheDocument();
    expect(breadcrumbsElement).toBeInTheDocument();
  });
});
