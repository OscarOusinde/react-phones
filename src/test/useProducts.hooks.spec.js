import {renderHook} from '@testing-library/react';
import { describe, it, expect } from "@jest/globals";
import { useProducts } from '../hooks/useProducts.hooks';
import { act } from 'react-dom/test-utils';

describe('useProducts hook spec', () => {
    it('should return and array of products', () => {
        //Arrange

        //Act
        const {result} = renderHook(() => useProducts());

        //Assert
        expect(result.current.productsList).toHaveLength(100);
        expect(result.current.setProductsList).toEqual(expect.any(Function));
    })
    it('should update when it calls setProductsList', () => {
        //Arrange
        const newProductsList = {id: "test", brand: "samsung", model: "A50", price: "250"};

        //Act
        const {result} = renderHook(() => useProducts());

        act(() => {
            result.current.setProductsList(newProductsList);
        })

        //Assert
        expect(result.current.productsList).toEqual(newProductsList);
    })
})