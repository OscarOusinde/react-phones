import {renderHook} from '@testing-library/react';
import { describe, it, expect } from "@jest/globals";
import { useSingleProduct } from '../hooks/useSingleProduct.hooks';
import { act } from 'react-dom/test-utils';

describe('useSingleProduct hook specs', () => {
    it('should call a single product', () => {
        //Arrange
        //HERE WE SHOULD DISPLAY A CONST WITH THE ID TO PASS TO THE HOOK AND MOCK THE API CALL
        const id = 'YXchmkgzbCvPPUFnViAbT';

        //Act
        const {result} = renderHook(() => useSingleProduct());

        //Assert
        //HERE WE SHOULD EXPECT THE RETURN OF THE SAME ID FROM API
        expect(result.current.singleProduct.id).toBe(id);
        expect(result.current.setSingleProduct).toEqual(expect.any(Function))
    });
    it('should update singleProduct when call setSingleProduct', () => {
        //Arrange
        const newProduct = {id: "test", brand: "samsung", model: "A50", price: "250"};

        //Act
        const {result} = renderHook(() => useSingleProduct());
        act(() => {
            result.current.setSingleProduct(newProduct);
        })

        //Assert
        expect(result.current.singleProduct).toEqual(newProduct);
    })
})