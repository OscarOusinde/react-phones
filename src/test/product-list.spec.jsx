import React from "react";
import { describe, it, expect } from "@jest/globals";
import { render, screen, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Products } from "../components/ProductsList";
import { HashRouter, Routes, Route } from "react-router-dom";
import products from "../fixtures/products.json";
import { ProductProvider } from "../contexts/ProductContext";

const renderWithRouter = (component) => {
  render(
    <ProductProvider>
      <HashRouter>
        <Routes>
          <Route path="/" element={component} />
          <Route path="/products/:productName/:productId" element={component} />
        </Routes>
      </HashRouter>
    </ProductProvider>
  );
};

describe("Product List specs", () => {
  it("should display the products", () => {
    //Arrange
    const productsListLenght = products.length;

    //Act
    renderWithRouter(<Products />);

    //Assert
    const list = screen.getAllByRole("listitem");
    const listElement = screen.getByText("Iconia Talk S");

    expect(list).toHaveLength(productsListLenght);
    expect(listElement).toBeInTheDocument();
  });
  it("should navigate to the propduct", () => {
    //Arrange
    const product = "Iconia Talk S";

    //Act
    renderWithRouter(<Products />);

    //Assert
    const productToClick = screen.getByText(product);
    fireEvent.click(productToClick);

    expect(window.location.hash).toBe(
      "#/products/Iconia%20Talk%20S/ZmGrkLRPXOTpxsU4jjAcv"
    );
  });
});
