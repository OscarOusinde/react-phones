import React from "react";
import { HashRouter, Routes, Route } from "react-router-dom";
import { Products } from "./ProductsList";
import { ProductDetail } from "./ProductDetail";

export const Router = () => {
  return (
    <HashRouter>
      <Routes>
        <Route path="/" element={<Products />} />
        <Route
          path="/products/:productName/:productId"
          element={<ProductDetail />}
        />
      </Routes>
    </HashRouter>
  );
};
