import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useFilter } from "../hooks/useFilter.hooks";

const SearchContainer = styled.aside`
  padding: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  gap: 0.5rem;
  @media (min-width: 1000px) {
    justify-content: flex-end;
    padding: 1rem 3rem;
  }
`;
const SearchInput = styled.input`
  padding: 0.5rem;
  font-size: 1.2rem;
  border: 1px solid #ccc;
  border-radius: 4px;
  width: 60vw;
  max-width: 500px;
  font-family: "Montserrat", sans-serif;

  @media (min-width: 1000px) {
    justify-self: flex-end;
  }
`;

export const Search = ({ setProducts, productsList }) => {
  const [search, setSearch] = useState("");

  const filteredProducts = useFilter(search, productsList);

  useEffect(() => {
    setProducts(filteredProducts);
  }, [search]);

  return (
    <SearchContainer>
      <SearchInput
        type="text"
        placeholder="Product to search..."
        value={search}
        onChange={(e) => setSearch(e.target.value)}
      />
    </SearchContainer>
  );
};
