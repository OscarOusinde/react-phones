import React, { useState } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Header } from "./Header";
import { Search } from "./Search";
import { useProducts } from "../hooks/useProducts.hooks";

const MainProducts = styled.main`
  display: flex;
  justify-content: center;
`;

const ProductsContainer = styled.section`
  height: 100vh;
  padding: 1rem;
  @media (min-width: 1200px) {
    width: 100vw;
    max-width: 90vw;
  }
`;

const ProductsList = styled.ul`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  gap: 1.5rem;
`;

const ProductsListLi = styled.li`
  box-shadow: 0 6px 6px #f0f0f0;
  padding: 1rem;
  border-radius: 4px;
  border: 1px solid #f0f0f0;
  width: 220px;
  height: 300px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const ProductImage = styled.img`
  width: 220px;
  cursor: pointer;
`;

const ProductData = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.5rem;
  font-family: "Montserrat", sans-serif;
`;

const ProductModel = styled.p`
  font-size: 1.8rem;
  text-align: center;
  font-weight: 500;
`;

const ProductBrand = styled.p`
  font-size: 1.5rem;
  text-align: center;
`;

const ProductPrice = styled.p`
  font-size: 1.2rem;
  text-align: center;
`;

const NoProductsContainer = styled.section`
  display: flex;
  flex-direction: column;
  gap: 1rem;
  text-align: center;
  font-family: "Montserrat", sans-serif;
`;
const Sorry = styled.p`
  font-size: 2rem;
  font-weight: 600;
`;
const NotProducts = styled.p`
  font-size: 1.5rem;
`;

export const Products = () => {
  const { productsList } = useProducts();

  const [products, setProducts] = useState(productsList);

  return (
    <>
      <Header />
      <Search setProducts={setProducts} productsList={productsList} />
      <MainProducts>
        <ProductsContainer>
          <ProductsList>
            {products.length === 0 ? (
              <NoProductsContainer>
                <Sorry>Sorry</Sorry>
                <NotProducts>
                  No products available. Please, try another search
                </NotProducts>
              </NoProductsContainer>
            ) : (
              products.map((product) => {
                return (
                  <Link
                    key={product.id}
                    to={`/products/${product.model}/${product.id}`}
                  >
                    {
                      <ProductsListLi>
                        <ProductImage
                          src="../src/assets/images/phone.jpg"
                          alt={product.model}
                        />
                        <ProductData>
                          <ProductModel>{product.model}</ProductModel>
                          <ProductBrand>{product.brand}</ProductBrand>
                          <ProductPrice>
                            {product.price ? product.price : 200}$
                          </ProductPrice>
                        </ProductData>
                      </ProductsListLi>
                    }
                  </Link>
                );
              })
            )}
          </ProductsList>
        </ProductsContainer>
      </MainProducts>
    </>
  );
};
