import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import { useParams } from "react-router-dom";
import { Header } from "./Header";
import { useSingleProduct } from "../hooks/useSingleProduct.hooks";
import { v4 as uuidv4 } from "uuid";
import { ProductContext } from "../contexts/ProductContext";

const ProductContainer = styled.section`
  padding: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  gap: 2rem;
`;

const ProductImage = styled.img`
  height: 230px;
  cursor: pointer;
  @media (min-width: 1000px) {
    height: 330px;
  }
`;

const ProductInfoContainer = styled.aside`
  display: flex;
  flex-direction: column;
  gap: 2rem;
`;

const Model = styled.li`
  font-size: 2rem;
  font-family: "Montserrat", sans-serif;
`;
const Brand = styled.li`
  font-size: 1.5rem;
  font-family: "Montserrat", sans-serif;
`;
const Price = styled.li`
  font-size: 1.5rem;
  align-self: flex-end;
  margin-right: 2rem;
  font-family: "Montserrat", sans-serif;
  font-weight: 600;
`;
const ProductData = styled.ul`
  display: flex;
  flex-direction: column;
  gap: 0.5rem;
`;

const SectionTitle = styled.p`
  font-size: 1.5rem;
  font-family: "Montserrat", sans-serif;
  font-weight: 500;
`;

const DetailsLi = styled.li`
  font-size: 1.2rem;
  margin-bottom: 0.8rem;
`;
const ProductDetails = styled.ul`
  margin: 1rem;
  margin-top: -1rem;
  font-family: "Montserrat", sans-serif;
  list-style: disc;
`;

const ProductSelectors = styled.section`
  padding: 1rem;
  display: flex;
  flex-direction: column;
  max-width: 500px;
  margin: 0 auto;
  @media (min-width: 1050px) {
    align-items: flex-end;
  }
`;
const Label = styled.label`
  display: block;
  font-family: "Montserrat", sans-serif;
  font-size: 1.2rem;
  margin: 1rem 0;
  @media (min-width: 1050px) {
    margin: 1rem auto;
  }
`;
const InternalMemory = styled.select`
  font-family: "Montserrat", sans-serif;
  font-size: 1.2rem;
  width: 60vw;
  max-width: 250px;
  padding: 0.5rem;
  align-self: center;
  border-radius: 4px;
  @media (min-width: 1050px) {
    align-self: flex-end;
  }
`;
const Color = styled.select`
  font-family: "Montserrat", sans-serif;
  font-size: 1.2rem;
  width: 60vw;
  max-width: 250px;
  padding: 0.5rem;
  align-self: center;
  border-radius: 4px;
  @media (min-width: 1050px) {
    align-self: flex-end;
  }
`;

const AddButton = styled.button`
  width: 60vw;
  max-width: 300px;
  height: 3rem;
  margin: 2rem auto;
  display: block;
  font-family: "Montserrat", sans-serif;
  font-size: 1.5rem;
  font-weight: 600;
  border: none;
  background-color: #00aaee;
  color: #fff;
  border-radius: 10px;
  box-shadow: 0 6px 6px #f0f0f0;
  border: 1px solid #f0f0f0;
  cursor: pointer;
  @media (min-width: 1050px) {
    align-self: flex-end;
  }
`;

export const ProductDetail = () => {
  //The useSingleProduct should have the productId as parameter to call the API
  const { singleProduct } = useSingleProduct();

  //Use params
  const { productName, productId } = useParams();

  //States
  const [memory, setMemory] = useState(singleProduct.internalMemory[0]);
  const [color, setColor] = useState(singleProduct.colors[0]);

  const [selection, setSelection] = useState({ id: productId, color, memory });
  useEffect(() => {
    setSelection({ id: productId, color, memory });
  }, [memory, color, productId]);

  //Context
  const { addProduct } = useContext(ProductContext);

  return (
    <>
      <Header />

      <ProductContainer>
        <ProductImage
          data-testid="product-image"
          src="../src/assets/images/phone.jpg"
          alt={productName}
        />
        <ProductInfoContainer>
          <ProductData>
            <Model>{productName}</Model>
            <Brand>{singleProduct.brand}</Brand>
            <Price>{singleProduct.price}$</Price>
          </ProductData>
          <SectionTitle>Product details:</SectionTitle>
          <ProductDetails>
            <DetailsLi>CPU: {singleProduct.cpu}</DetailsLi>
            <DetailsLi>RAM: {singleProduct.ram}</DetailsLi>
            <DetailsLi>OS: {singleProduct.os[0]}</DetailsLi>
            <DetailsLi>Screen: {singleProduct.displayResolution}</DetailsLi>
            <DetailsLi>Battery: {singleProduct.battery}</DetailsLi>
            <DetailsLi>Primary camera: {singleProduct.primaryCamera}</DetailsLi>
            <DetailsLi>
              Secondary camera: {singleProduct.secondaryCmera}
            </DetailsLi>
            <DetailsLi>Dimentions: {singleProduct.dimentions}</DetailsLi>
            <DetailsLi>Weight: {singleProduct.weight}</DetailsLi>
          </ProductDetails>
        </ProductInfoContainer>
      </ProductContainer>
      <ProductSelectors>
        <SectionTitle>Select your configuration:</SectionTitle>
        <Label htmlFor="memory">Memory:</Label>
        <InternalMemory
          name="memory"
          onChange={(e) => setMemory(e.target.value)}
          value={memory}
        >
          <option>{singleProduct.internalMemory[0]}</option>
        </InternalMemory>
        <Label htmlFor="color">Color:</Label>
        <Color
          name="color"
          onChange={(e) => setColor(e.target.value)}
          value={color}
        >
          {singleProduct.colors.map((color) => {
            return <option key={uuidv4()}>{color}</option>;
          })}
        </Color>
      </ProductSelectors>
      <AddButton onClick={() => addProduct(selection)}>Add</AddButton>
    </>
  );
};
