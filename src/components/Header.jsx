import React, { useContext, useEffect, useState } from "react";
import styled from "styled-components";
import { Link, useParams } from "react-router-dom";
import { ProductContext } from "../contexts/ProductContext";

const HeaderContainer = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 1rem;
  background-color: #f0f0f0;
  font-family: "Montserrat", sans-serif;
  @media (min-width: 1200px) {
    padding: 2rem 4rem;
  }
`;

const Logo = styled.img`
  cursor: pointer;
`;

const Breadcrumbs = styled.nav`
  font-size: 1.2rem;
  padding: 1rem;
  font-family: "Montserrat", sans-serif;
  display: flex;
  gap: 1rem;
  @media (min-width: 1200px) {
    padding: 1rem 4rem;
    font-size: 1.5rem;
  }
`;

const Separator = styled.span``;

const BreadcrumbProduct = styled.p`
  display: inline;
  color: grey;
`;

const ProductCount = styled.span`
  font-size: 1.5rem;
  display: flex;
  gap: 0.5rem;
  justify-content: center;
  align-items: center;
  @media (min-width: 1200px) {
    font-size: 2rem;
  }
`;

const ShoppingCart = styled.img`
  width: 1.8rem;
  @media (min-width: 1200px) {
    width: 2.5rem;
  }
`;

export const Header = () => {
  //Params
  const params = useParams();

  //Context
  const { products } = useContext(ProductContext);
  const [numberProducts, setNumberProducts] = useState(
    products !== null ? products.length : 0
  );

  useEffect(() => {
    setNumberProducts(products !== null ? products.length : 0);
  }, [products]);

  return (
    <>
      <HeaderContainer>
        <Link to="/">
          <Logo src="../src/assets/logo/logo.svg" alt="logoIpsum" />
        </Link>
        <ProductCount data-testid="product-count">
          {numberProducts}
          <ShoppingCart src="../src/assets/icons/cart.png" />
        </ProductCount>
      </HeaderContainer>
      <Breadcrumbs>
        <Link to="/">Home</Link>
        <Separator>{params.productName ? ">" : ""}</Separator>
        <BreadcrumbProduct>
          {params.productName ? params.productName : ""}
        </BreadcrumbProduct>
      </Breadcrumbs>
    </>
  );
};
