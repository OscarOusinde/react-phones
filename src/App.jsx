// import { Products } from "./components/ProductsList";
import { Router } from "./components/Router";
import { ProductProvider } from "./contexts/ProductContext";
import "./reset.css";
import "./styles.css";

function App() {
  return (
    <>
      <ProductProvider>
        <Router />
      </ProductProvider>
    </>
  );
}

export default App;
