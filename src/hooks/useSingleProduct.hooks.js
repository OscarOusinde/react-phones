import React, { useState } from "react";
import product from '../fixtures/single-product.json'

export const useSingleProduct = () => {

    //THIS HOOK SHOULD CALL THE API WITH A PRODUCT ID BY PROP
    const [singleProduct, setSingleProduct] = useState(product);

    return (
        {
            singleProduct,
            setSingleProduct,
        }
    )
}