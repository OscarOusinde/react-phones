import React, { useEffect, useState } from "react";
import products from '../fixtures/products.json';

export const useProducts = () => {
    const [productsList, setProductsList] = useState(products)

    return (
        {
            productsList,
            setProductsList,
        }
        
    )
}