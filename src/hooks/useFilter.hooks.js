
export const useFilter = (search , productsList) => {

     const filteredProducts = productsList.filter((product) => {
        const modelMatch = product.model.toLowerCase().startsWith(search);
        const brandMatch = product.brand.toLowerCase().startsWith(search);
        return modelMatch || brandMatch;
    });

    return filteredProducts;
      
}