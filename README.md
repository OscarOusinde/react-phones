# Phones

Phones it's an APP designed as a mobile phone shop. You can visualize all the phones available with their model, brand and price.

![Texto alternativo](./Snapshots/Home-computer.png)

You can filter the products by model/brand on the search bar at the top. For example by typing 'al' you will get:

![Texto alternativo](./Snapshots/Home-computer-filter.png)

By selecting a product you can find more details about it:

![Texto alternativo](./Snapshots/Details-computer.png)

Here you can choose the color and memory of the phone selected. Currently there is only one option for memory:

![Texto alternativo](./Snapshots/Details-computer-options.png)

By clicking on Add you will add the product to the cart and see the number of product growthing.

## Starting the app

- `npm install` to install all the necessary dependencies.
- `npm start` to start development mode and visualice the app in your browser.
- By using `npm run test` you can run the test suites. Also with `npm run test:watch`.
